extern crate log;
extern crate log4rs;
extern crate sentry;

use log::{Level, Record};
use log4rs::{
	append::Append,
	encode::{
		Encode,
		writer::simple::SimpleWriter
	}
};
use sentry::{
	internals::ClientInitGuard,
	protocol::value::{Number, Value},
	Client as SentryClient,
	Level as SentryLevel
};
use std::{
	fmt,
	fmt::{Debug, Formatter}
};

pub struct SentryAppender
{
	encoder : Box<Encode>,
	_sentry : Option<ClientInitGuard>
}

impl SentryAppender
{
	pub fn new(encoder : Box<Encode>) -> Self
	{
		SentryAppender { encoder, _sentry: None }
	}

	pub fn with_sentry_cfg<C : Into<SentryClient>>(mut self, cfg : C)
	{
		self._sentry = Some(sentry::init(cfg));
	}
}

impl Debug for SentryAppender
{
	fn fmt(&self, f : &mut Formatter) -> fmt::Result
	{
		write!(f, "SentryAppender{{...}}")
	}
}

impl Append for SentryAppender
{
	fn append(&self, record: &Record) -> Result<(), Box<std::error::Error + Sync + Send>>
	{
		let level = match record.level() {
			Level::Error => SentryLevel::Error,
			Level::Warn => SentryLevel::Warning,
			_ => return Ok(()) // don't send messages < warn to sentry
		};
		
		let mut buf : Vec<u8> = Vec::new();
		self.encoder.encode(&mut SimpleWriter(&mut buf), record)?;
		let msg = match String::from_utf8(buf) {
			Ok(msg) => msg,
			Err(e) => return Err(Box::new(e))
		};
		
		let mut event = sentry::protocol::Event::new();
		event.level = level;
		event.message = Some(msg);
		event.logger = Some(record.metadata().target().to_owned());
		
		if let Some(file) = record.file()
		{
			event.extra.insert("file".to_owned(), Value::String(file.to_owned()));
		}
		
		if let Some(line) = record.line()
		{
			event.extra.insert("line".to_owned(), Value::Number(Number::from(line)));
		}
		
		if let Some(module_path) = record.module_path()
		{
			event.tags.insert("module_path".to_owned(), module_path.to_owned());
		}
		
		sentry::capture_event(event);
		Ok(())
	}

	fn flush(&self) {}
}
